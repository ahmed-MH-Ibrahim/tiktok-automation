

window.addEventListener("load", function(event) {

	//start initializing tiktok window and scripts
	$.get(chrome.runtime.getURL("html/tiktok-window.html"), function(data){

		//append tiktok window to website UI
	    $("body").append(data);  


	    //main extension script

	    //load vuejs script first otherwise jquery event handlers dont work on added DOM
	    loadVueJsScripts();

	    //load additional scripts here
	    loadScripts();

	    //load stored variables to continue session
	    init();

	});
});

//load additional scripts here
function loadScripts(){

	
}

//inits session data from previously stored sessions
function init(){

	chrome.storage.local.get(loadUserParameters,function(obj){
      	if(jQuery.isEmptyObject(obj))
      	{
      		//set client name with tiktok profile name if they are logged in to tiktok
      		if(vm.isTiktokLoggedIn())
      		{	          
	          vm.user.client = getProfileNameOnTikTok();          
      		}

          	return;      	
      	}

      	if(obj.lastDataFetchTimestamp != undefined)
  		{
  			vm.lastDataFetchTimestamp = moment(obj.lastDataFetchTimestamp);
  		}

  		if(obj.currentActiveVideoIndex != undefined)
  		{
  			vm.currentActiveVideoIndex = obj.currentActiveVideoIndex;
  		}

  		if(obj.currentVideoScore != undefined)
  		{
  			vm.currentVideoScore = obj.currentVideoScore;
  		}

  		if(obj.profile != undefined)
  		{
  			vm.profile = obj.profile;
  		}

  		if(obj.tiktokVideos != undefined)
  		{
  			vm.tiktokVideos = obj.tiktokVideos;
  		}

  		if(obj.extEnabled != undefined)
  		{
  			vm.extEnabled = obj.extEnabled;
  			vm.disableText = !vm.extEnabled ? enableExtBtnText:disableExtBtnText;
  		}

  		if(obj.mostLikedComments != undefined)
  		{
  			vm.mostLikedComments = obj.mostLikedComments;
  		}

  		//if user previously logged in
  		//checking login last to start video redirect after loading all data.
      	if(obj.loggedIn)
  		{
  			vm.loggedIn = obj.loggedIn;
  			vm.user.name = obj.user.name;
  			vm.user.client = obj.user.client;
  			vm.user.lang = obj.user.lang;

  			//user logged in, redirect to first video they should start with
  			vm.loadCurrentVideo(); 
  		}  		

    });

	//every minute check if data is outdated
    checkOutDatedDataInterval = setInterval(function(){
    	if(vm.lastDataFetchTimestamp != "")
    	{	
    		var now = moment();
    		//var lastCheck = moment(vm.lastDataFetchTimestamp.format(""), "DD/MM/YYYY HH:mm:ss");
    		var diff = now.diff(vm.lastDataFetchTimestamp);
    		var duration = moment.duration(diff).asMinutes();

    		if(duration >= 30)
    		{
    			//reload data
    			vm.loadVideos();
    		}
    	}
    	
    },1000)
}