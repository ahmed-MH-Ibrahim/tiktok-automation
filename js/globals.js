//global vuejs handler
var vm;
var checkOutDatedDataInterval;
var loadUserParameters = ["token","user","loggedIn","currentActiveVideoIndex","profile", "tiktokVideos"/*,"mostLikedComments"*/,"currentVideoScore","extEnabled","lastDataFetchTimestamp"];
var serverUrl = "https://app.infludata.com/api/externalAPI/";
var accessToken = "ExEQPNRStRxkPaJcCb76rdNUtKysMge8";
var mostLikedCommentTitle = "Comments & Ranking";
var disableExtBtnText = "Disable auto scroll";
var enableExtBtnText = "Enable auto scroll";
var serverError = "Server Error";
var noVideosTitle = "No Videos Available";
var noVideosMsg = "No videos found. Please go back and change your settings.";
var commentLimitReachedTitle = "You have reached your comment limit";
var commentLimitReached = "Please contact the influData Support. Thank you!";
var notAuthenticatedMsgTitle = "You are not authenticated";
var notAuthenticatedMsg = "Please contact the influData Support. Thank you!";


//retrieves profile name on tiktok
function getProfileNameOnTikTok(){
	var tiktokData = $("#sigi-persisted-data").text();
	var nickNameField = tiktokData.substring(tiktokData.indexOf('uniqueId'));
	nickNameField = nickNameField.substring(0, nickNameField.indexOf('\",'));
	nickNameField = nickNameField.substring(nickNameField.indexOf(':')+1);
	nickNameField = nickNameField.replaceAll('\"','');

	return nickNameField;
}