$(document).ready(function(){

  vm = new Vue({
    el: '#ranking-titkok-container',
    data: {
      profile:{
        comments_today: 0,
        comments_overall:0,
        comments_likes_today:0,
        comments_likes_overall:0,
        mostLikesArray_USER_24H:[],
        mostLikesArray_USER_ALL:[],
        mostLikesArray_USER_30D:[],
        mostLikesArray_COMPANY_30D:[],
        mostLikesArray_COMPANY_24H:[],
        mostLikesArray_COMPANY_ALL:[],
        rankingArray_30D:[],
        rankingArray_24H:[],
        rankingArray_ALL:[],        
      },
      mostLikesArray_USER_24H_on:false,
      mostLikesArray_USER_30D_on:false,
      mostLikesArray_USER_ALL_on:true,
      mostLikesArray_COMPANY_24H_on:false,
      mostLikesArray_COMPANY_30D_on:false,
      mostLikesArray_COMPANY_ALL_on:true,
      rankingArray_24H_on:false,
      rankingArray_30D_on:false,
      rankingArray_ALL_on:true,
      allTablesInit: false,
      tiktokVideos:[],
      mostLikedComments:[],
      currentActiveVideoIndex:-1,
      comment:'',
    },
    mounted: function () {    
      chrome.storage.local.get(loadUserParameters,function(obj){
        if(jQuery.isEmptyObject(obj))
        {
          

          return;       
        }

        if(obj.profile != undefined)
        {
          vm.profile = obj.profile;                    
        }




      });

      
    },
    updated(){


    },
    watch: {

    },
    computed :{

    },
    filters: {
      
      kFormatter: function(num) {
          return Math.abs(num) > 999 ? Math.sign(num)*((Math.abs(num)/1000).toFixed(1)) + 'k' : Math.sign(num)*Math.abs(num)
      },
      displayDate: function(date)
      {
        //return moment.unix(date).format("MM/DD/YYYY");
        return moment(new Date(date)).format("MMMM Qo, YYYY, hh:mma ");
      }
    },
    methods: { 
      show_user_all:function(){
        vm.mostLikesArray_USER_24H_on = false;
        vm.mostLikesArray_USER_30D_on = false;
        vm.mostLikesArray_USER_ALL_on = true;
      },
      show_user_30d:function(){
        vm.mostLikesArray_USER_24H_on = false;
        vm.mostLikesArray_USER_30D_on = true;
        vm.mostLikesArray_USER_ALL_on = false;
      },
      show_user_24h:function(){
        vm.mostLikesArray_USER_24H_on = true;
        vm.mostLikesArray_USER_30D_on = false;
        vm.mostLikesArray_USER_ALL_on = false;
      },
      show_company_all:function(){
        vm.mostLikesArray_COMPANY_24H_on = false;
        vm.mostLikesArray_COMPANY_30D_on = false;
        vm.mostLikesArray_COMPANY_ALL_on = true;
      },
      show_company_30d:function(){
        vm.mostLikesArray_COMPANY_24H_on = false;
        vm.mostLikesArray_COMPANY_30D_on = true;
        vm.mostLikesArray_COMPANY_ALL_on = false;
      },
      show_company_24h:function(){
        vm.mostLikesArray_COMPANY_24H_on = true;
        vm.mostLikesArray_COMPANY_30D_on = false;
        vm.mostLikesArray_COMPANY_ALL_on = false;
      },
      show_ranking_all:function(){
        vm.rankingArray_ALL_on = false;
        vm.rankingArray_30D_on = false;
        vm.rankingArray_ALL_on = true;
      },
      show_ranking_30d:function(){
        vm.rankingArray_ALL_on = false;
        vm.rankingArray_30D_on = true;
        vm.rankingArray_ALL_on = false;
      },
      show_ranking_24h:function(){
        vm.rankingArray_ALL_on = true;
        vm.rankingArray_30D_on = false;
        vm.rankingArray_ALL_on = false; 
      },     
    }
  })

  $(".filter-btn").click(function(){
    $(".filter-active").removeClass("filter-active");
    $(this).addClass("filter-active");
  })

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href") // activated tab

    if($(target+" .filter-active").length)
      return;

    $(target+" .filter-btn").first().trigger("click");
  });


})