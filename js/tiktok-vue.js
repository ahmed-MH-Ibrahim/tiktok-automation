

//method to handle loading vuejs session
function loadVueJsScripts(){

  vm = new Vue({
      el: '#ext-tiktok-window-container',
      data: {
        loggedIn: false,
        languages:{"en" : "EN", "de": "DE"},
        domInitialized:false,
        errored: false,
        errorMsg:'',
        succeeded: false,
        successMsg:'',
        messages:[],
        currentVideoScore: "-",
        extEnabled: true,
        disableText: disableExtBtnText,
        lastDataFetchTimestamp:'',
        logo:chrome.runtime.getURL("img/logo-tool.jpg"),
        logoShort:chrome.runtime.getURL("img/logo.png"),
        user:{
          name:"",
          client:"",
          lang:"en",
        },
        profile:{
          comments_today: 0,
          comments_overall:0,
          comments_likes_today:0,
          comments_likes_overall:0,
          mostLikesArray_USER_24H:0,
          mostLikesArray_USER_ALL:0,
          mostLikesArray_USER_30D:0,
          mostLikesArray_COMPANY_30D:0,
          mostLikesArray_COMPANY_24H:0,
          mostLikesArray_COMPANY_ALL:0,
          rankingArray_30D:0,
          rankingArray_24H:0,
          rankingArray_ALL:0,
        },
        tiktokVideos:[],
        mostLikedComments:[],
        currentActiveVideoIndex:-1,
        comment:'',
      },
      mounted: function () {    
        
      },
      updated(){

        if(!vm.domInitialized && !vm.loggedIn)
        {          

          //key 'm' on tikotok plays the current video. spacebar jumps to next
          //overriding letter 'm' and 'spacebar' keypress on extension form fields
          $( "#ext-tiktok-window-container input" ).keydown(function( event ) {

            //if key 'm' pressed
            if ( event.which == 77 ) 
            {
              event.preventDefault();
              event.stopPropagation();

              var letter = 'm';

              if (event.shiftKey==1)
                letter = 'M';

              if($(this).attr("id") == "name")
                vm.user.name = $(this).val()+letter;
              else if($(this).attr("id") == "client")
                vm.user.client = $(this).val()+letter;

            }
            elseif ( event.which == 32 ) 
            {
              event.preventDefault();
              event.stopPropagation();

              var letter = ' ';

              if($(this).attr("id") == "name")
                vm.user.name = $(this).val()+letter;
              else if($(this).attr("id") == "client")
                vm.user.client = $(this).val()+letter;

            }

          });

          vm.domInitialized = true;
        }
                    
      },
      watch: {
        loggedIn: function(val, oldVal){

        },
        extEnabled: function(){

          vm.disableText = !vm.extEnabled ? enableExtBtnText:disableExtBtnText;

          //store current disable status
          chrome.storage.local.set({"extEnabled":vm.extEnabled});

          //reload Data once enabled
          if(vm.extEnabled)
            vm.loadVideos();
        },
      },
      computed :{

      },
      methods: { 
        disableExtension: function(){
          vm.extEnabled = !vm.extEnabled;
          vm.disableText = !vm.extEnabled ? enableExtBtnText:disableExtBtnText;

          //store current disable status
          chrome.storage.local.set({"extEnabled":vm.extEnabled});

          //reload Data once enabled
          if(vm.extEnabled)
            vm.loadVideos();
        },
        hideMessage: function(){

          vm.errorMsg = "";
          vm.errored = false;
          vm.successMsg = "";
          vm.succeeded = false;

        },    
        isTiktokLoggedIn: function(){
          //check if user is logged in on tiktok
          var isloggedIn = false;

          if($("button[data-e2e='top-login-button']").length == 0)
          {
            //user is logged in
            isloggedIn = true;
          }

          return isloggedIn;

        },  
        login: function(){

          //check if logged in on tiktokfirst
          if(!vm.isTiktokLoggedIn())
          {
            vm.errorMsg = "Please login to Tiktok first";
            vm.errored = true;
            return;
          }

          if(vm.user.name.trim() != "" && vm.user.client.trim() != "" && vm.user.lang != "")
          {
            vm.loggedIn = true;
            vm.hideMessage();

            //store data
            chrome.storage.local.set({"user":vm.user}); 
            chrome.storage.local.set({"loggedIn":true});

            //make call to api
            vm.loadVideos();
          }
          else{
            vm.errorMsg = "Please fill all fields below";
            vm.errored = true;
          }
          
        },
        storeSessionData: function(){
          chrome.storage.local.set({"user":vm.user}); 
          chrome.storage.local.set({"profile":vm.profile}); 
          chrome.storage.local.set({"tiktokVideos":vm.tiktokVideos}); 
          chrome.storage.local.set({"mostLikedComments":vm.mostLikedComments}); 
          chrome.storage.local.set({"currentActiveVideoIndex":vm.currentActiveVideoIndex}); 
          chrome.storage.local.set({"currentVideoScore":vm.currentVideoScore}); 
          chrome.storage.local.set({"extEnabled":vm.extEnabled}); 
        },
        loadVideos: function(){

            $(".loading-div").show();
            //load first 10 videos
            //This is called when all 10 videos are parsed or user logged in for the first time
            axios
            .get(serverUrl+'commentApiGet?accessToken='+accessToken+'&employee='+encodeURIComponent(vm.user.name)+'&lang='+encodeURIComponent(vm.user.lang)+'&client='+encodeURIComponent(vm.user.client))
            .then(response => {
 
                console.log(response);

                if(response.status == "401")
                {
                  swal.fire({
                    title: notAuthenticatedMsgTitle,
                    icon: "error",
                    text: notAuthenticatedMsg,
                  });

                  return;
                }
                else if(response.status == "429")
                {
                  swal.fire({
                    title: commentLimitReachedTitle,
                    icon: "error",
                    text: commentLimitReached,
                  });

                  return;
                }
                else if(response.status != "200")
                {
                  swal.fire({
                    title: serverError,
                    icon: "error",
                    text: JSON.stringify(response),
                  });

                  return;
                }                 
                else if(response.data != undefined && (response.data.listArray == undefined || response.data.listArray.length == 0))
                {
                  //no videos returned
                  swal.fire({
                    title: noVideosTitle,
                    icon: "warning",
                    text: noVideosMsg,
                  });

                  return;
                }

                var userData = response.data.userdata; 
                var videoList = response.data.listArray;

                vm.profile.comments_today = userData.commentsToday;
                vm.profile.comments_overall = userData.commentsOverall;
                vm.profile.comments_likes_today = userData.commentsLikesToday;
                vm.profile.comments_likes_overall = userData.commentsLikesOverall;
                vm.profile.mostLikesArray_USER_24H = userData.mostLikesArray_USER_24H;
                vm.profile.mostLikesArray_USER_ALL = userData.mostLikesArray_USER_ALL;
                vm.profile.mostLikesArray_USER_30D = userData.mostLikesArray_USER_30D;
                vm.profile.mostLikesArray_COMPANY_30D = userData.mostLikesArray_COMPANY_30D;
                vm.profile.mostLikesArray_COMPANY_24H = userData.mostLikesArray_COMPANY_24H;
                vm.profile.mostLikesArray_COMPANY_ALL = userData.mostLikesArray_COMPANY_ALL;
                vm.profile.rankingArray_30D = userData.rankingArray_30D;
                vm.profile.rankingArray_24H = userData.rankingArray_24H;
                vm.profile.rankingArray_ALL = userData.rankingArray_ALL;

                vm.tiktokVideos = videoList;

                for (var i = vm.tiktokVideos.length - 1; i >= 0; i--) {
                  vm.tiktokVideos[i]["isParsed"] = false;
                }

                //store timestamp of last data update
                vm.lastDataFetchTimestamp = moment();
                chrome.storage.local.set({"lastDataFetchTimestamp":vm.lastDataFetchTimestamp.toISOString()});

                vm.currentActiveVideoIndex = -1;
                vm.parseNextVideo();

                
            })
            .catch(error => {

                $(".loading-div").hide();
                console.log(error.response);            

            })
            .finally(() => $(".loading-div").hide())
        },
        loadCurrentVideo:function(){
          //if not at video, redirect 
          if(window.location.href != vm.tiktokVideos[vm.currentActiveVideoIndex].link && vm.extEnabled)
          {
            window.location.href = vm.tiktokVideos[vm.currentActiveVideoIndex].link;
          }    

          //clicking to automatically start comment section view
          $("span[data-e2e='comment-icon']").first().parents("button").get(0).click(); 

          //higlighting comment section so user can start typing
          setTimeout(function(){
            $("div[data-e2e='comment-input'").attr("tabindex",-1).focus();
            $("div[data-e2e='comment-input'").get(0).click();

          }, 2000);

          //add handler for post button click
          $("div[data-e2e='comment-post']").click(function(){
            //get comment
            vm.comment = $("div[data-e2e='comment-text'] .DraftEditor-root").text().trim();

            vm.continue();
          })   

          //detect enter key press for comment posting
          $( "body" ).keydown(function( event ) {

            //if enter key pressed
            if ( event.which == 13 ) 
            {
              //get comment
              vm.comment = $("div[data-e2e='comment-text'] .DraftEditor-root").text().trim();

              if(vm.comment.trim() != "")
                vm.continue();

            }
          });
        },
        parseNextVideo: function(){

          //handle next video in the list
          if((vm.currentActiveVideoIndex+1) == vm.tiktokVideos.length)
          {
            //all videos parsed, fetch next videos
            vm.loadVideos();
            return;
          }

          vm.currentActiveVideoIndex++;
          vm.currentVideoScore = vm.tiktokVideos[vm.currentActiveVideoIndex].score;

          //store session before redirecting
          vm.storeSessionData();

          //store values and redirect to next video
          window.location.href = vm.tiktokVideos[vm.currentActiveVideoIndex].link;


        },
        logout: function(){
          vm.loggedIn = false;
          vm.domInitialized = false;

          //reset user data
          this.resetData();

          //remove user session
          chrome.storage.local.remove(loadUserParameters,function(){
           var error = chrome.runtime.lastError;
              if (error) {
                  console.error(error);
              }
          })

          //get client name from tiktok
          if(vm.isTiktokLoggedIn())
          {           
            vm.user.client = getProfileNameOnTikTok();          
          }
          
        },
        resetData: function(){
          vm.user.client = "";
          vm.user.name = "";
          vm.user.lang = "en";
        },
        updateServerWithUserAction: function(isSkipped = false)
        { 
          if(isSkipped)
            vm.comment = "";  

          //get influencer from url
          var link = vm.tiktokVideos[vm.currentActiveVideoIndex].link;  
          link = link.substring(link.indexOf("@") + 1);        
          var influencer = link.substring(0, link.indexOf("/"));
          
          //uncomment to get influencer name from browser instead of url
          //var influencer = $("span[data-e2e='browse-username']").text();

          $(".loading-div").show();

          axios
            .post(serverUrl+'commentApiAction?accessToken='+accessToken+'&employee='+encodeURIComponent(vm.user.name)+'&client='+encodeURIComponent(vm.user.client)+"&comment="+encodeURIComponent(vm.comment)+"&isSkipped="+isSkipped+"&postId="+vm.tiktokVideos[vm.currentActiveVideoIndex].postId+"&influencer="+encodeURIComponent(influencer), {})
            .then(response => {

                console.log(response);

                if(response.status == "201")
                {
                  ///server updated, process to next video
                  vm.parseNextVideo(); 
                } 
                else if(response.status == "401")
                {
                  swal.fire({
                    title: notAuthenticatedMsgTitle,
                    icon: "error",
                    text: notAuthenticatedMsg,
                  });

                  return;
                }
                else if(response.status == "429")
                {
                  swal.fire({
                    title: commentLimitReachedTitle,
                    icon: "error",
                    text: commentLimitReached,
                  });

                  return;
                }
                else{
                  swal.fire({
                    title: serverError,
                    icon: "error",
                    text: JSON.stringify(response),
                  });
                }

                              
            })
            .catch(error => {
                console.log(error.response);       
            })
            .finally(() => $(".loading-div").hide())
        },
        skip: function(){
          console.log("skip button clicked");

          //make post request to update server then proceed
          vm.updateServerWithUserAction(true);

        },
        continue: function(){
          console.log("post button clicked");          

          //make post request to update server then proceed
          vm.updateServerWithUserAction();

        },
        showMostLikedPopup:function(){


          //send background command to open rankings page
          chrome.runtime.sendMessage({"command": "rankings-page"});

          /*var html = "";

          for (var i = 0; i < vm.mostLikedComments.length; i++) {
            html += '<div class="top-comment-row"><a class="top-comment-link" target="_blank" href="'+vm.mostLikedComments[i].link+'">'+vm.mostLikedComments[i].comment+'</a><div class="top-comment-attribute"> '+vm.mostLikedComments[i].likes+' Likes</div></div>';            

          }

          swal.fire({
            title: mostLikedCommentTitle,
            //icon: "success",
            html: html,
          });*/
        },
    }
  });
}
