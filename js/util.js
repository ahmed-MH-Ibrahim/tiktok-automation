function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function jsUcfirst(string) 
{
	if(string == undefined || string.trim() == "")
		return string;

    return string.charAt(0).toUpperCase() + string.slice(1);
}

function ucFirstAll(string)
{
	var splitStr = string.toLowerCase().split(' ');

   	for (var i = 0; i < splitStr.length; i++) {
       splitStr[i] = jsUcfirst(splitStr[i]);   
   	}

   return splitStr.join(' '); 
}

function kFormatterReverse(num,formatCommas = false) {
    if(num == undefined || num == "")
        return 0;

    if(num.toLowerCase().includes("k"))
    {
        thousands = num.split(".")[0].replace(/[^0-9]/g,'');
        hundreds = 0;
        if(num.split(".")[1] != undefined)
            hundreds = num.split(".")[1].replace(/[^\d]/g, '');

        num = thousands*1000+hundreds*100;
        if(formatCommas)
            num = num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    return parseInt(num);

}